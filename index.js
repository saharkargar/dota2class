window.onscroll = () => {
  const el = document.getElementById("nav-bar-bg");
  let bg = "rgba(7, 7, 7, 0)";
  let offset = window.pageYOffset;
  switch (true) {
    case offset == 0:
      el.style.backgroundColor = bg;
      el.classList.remove("bg-blur");
      break;
    case offset > 0 && offset < 300:
      el.style.backgroundColor = `rgba(7, 7, 7, ${offset / 600})`;
      el.classList.add("bg-blur");
      break;
    case offset >= 300:
      el.style.backgroundColor = "rgba(7, 7, 7, 0.5)";
      el.classList.add("bg-blur");
      break;
  }
};
const tabs = document.querySelectorAll(".tab-button");
console.log(tabs);

tabs.forEach((tab) => {
  tab.addEventListener("click", () => {
    tabs.forEach((active) => {
      active.classList.remove("active");
    });

    tab.classList.add("active");
  });
});

document.querySelector("#hambergur").addEventListener("click", () => {
  const overlay = document.querySelector(".overlay");
  document.querySelector(".responsive-menu").classList.add("open");
  overlay.style.display = "block";
});
document.querySelector(".overlay").addEventListener("click", () => {
    const overlay = document.querySelector(".overlay");
    overlay.style.display="none";
    document.querySelector(".responsive-menu").classList.remove("open");
});
